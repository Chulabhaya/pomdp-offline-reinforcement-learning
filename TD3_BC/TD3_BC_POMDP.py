import copy
import torch
import torch.nn as nn
import torch.nn.functional as F

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Actor(nn.Module):
    def __init__(self, state_dim, action_dim, max_action):
        super(Actor, self).__init__()

        self.linear1 = nn.Linear(state_dim, 256)
        self.lstm1 = nn.LSTM(256, 256)
        self.linear2 = nn.Linear(256, 256)
        self.linear3 = nn.Linear(256, action_dim)

        self.max_action = max_action

    def forward(self, state_history, seq_lengths):
        # Pass state history through first linear layer
        a = self.linear1(state_history)
        # Pack padded sequence for passing into LSTM, then unpack
        # Deals with variable sequence issue from padding
        a = nn.utils.rnn.pack_padded_sequence(a, seq_lengths, enforce_sorted=False)
        self.lstm1.flatten_parameters()
        a, _ = self.lstm1(a)
        a, a_unpacked_len = torch.nn.utils.rnn.pad_packed_sequence(a)
        # Pass through another linear layer
        a = F.relu(self.linear2(a))

        return self.max_action * torch.tanh(self.linear3(a))


class Critic(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Critic, self).__init__()

        # Q1 architecture
        self.linear1 = nn.Linear(state_dim + action_dim, 256)
        self.lstm1 = nn.LSTM(256, 256)
        self.linear2 = nn.Linear(256, 256)
        self.linear3 = nn.Linear(256, 1)

        # Q2 architecture
        self.linear4 = nn.Linear(state_dim + action_dim, 256)
        self.lstm2 = nn.LSTM(256, 256)
        self.linear5 = nn.Linear(256, 256)
        self.linear6 = nn.Linear(256, 1)

    def forward(self, state_history, action_history, seq_lengths):
        sa = torch.cat([state_history, action_history], 2)

        q1 = F.relu(self.linear1(sa))
        q1 = nn.utils.rnn.pack_padded_sequence(q1, seq_lengths, enforce_sorted=False)
        self.lstm1.flatten_parameters()
        q1, _ = self.lstm1(q1)
        q1, q1_unpacked_len = torch.nn.utils.rnn.pad_packed_sequence(q1)
        time_dimension = 0
        last_timestep_idx = (
            (seq_lengths - 1).view(-1, 1).expand(len(seq_lengths), q1.size(2))
        )
        last_timestep_idx = last_timestep_idx.unsqueeze(time_dimension).to(device)
        q1 = q1.gather(time_dimension, last_timestep_idx).squeeze(time_dimension)
        q1 = F.relu(self.linear2(q1))
        q1 = self.linear3(q1)

        q2 = F.relu(self.linear4(sa))
        q2 = nn.utils.rnn.pack_padded_sequence(q2, seq_lengths, enforce_sorted=False)
        self.lstm2.flatten_parameters()
        q2, _ = self.lstm2(q2)
        q2, q2_unpacked_len = torch.nn.utils.rnn.pad_packed_sequence(q2)
        time_dimension = 0
        last_timestep_idx = (
            (seq_lengths - 1).view(-1, 1).expand(len(seq_lengths), q2.size(2))
        )
        last_timestep_idx = last_timestep_idx.unsqueeze(time_dimension).to(device)
        q2 = q2.gather(time_dimension, last_timestep_idx).squeeze(time_dimension)
        q2 = F.relu(self.linear5(q2))
        q2 = self.linear6(q2)

        return q1, q2

    def Q1(self, state_history, action_history, seq_lengths):
        sa = torch.cat([state_history, action_history], 2)

        q1 = F.relu(self.linear1(sa))
        q1 = nn.utils.rnn.pack_padded_sequence(q1, seq_lengths, enforce_sorted=False)
        self.lstm1.flatten_parameters()
        q1, _ = self.lstm1(q1)
        q1, q1_unpacked_len = torch.nn.utils.rnn.pad_packed_sequence(q1)
        time_dimension = 0
        last_timestep_idx = (
            (seq_lengths - 1).view(-1, 1).expand(len(seq_lengths), q1.size(2))
        )
        last_timestep_idx = last_timestep_idx.unsqueeze(time_dimension).to(device)
        q1 = q1.gather(time_dimension, last_timestep_idx).squeeze(time_dimension)
        q1 = F.relu(self.linear2(q1))
        q1 = self.linear3(q1)

        return q1


class TD3_BC_POMDP(object):
    def __init__(
        self,
        state_dim,
        action_dim,
        max_action,
        discount=0.99,
        tau=0.005,
        policy_noise=0.2,
        noise_clip=0.5,
        policy_freq=2,
        alpha=2.5,
    ):

        self.actor = Actor(state_dim, action_dim, max_action).to(device)
        self.actor_target = copy.deepcopy(self.actor)
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(), lr=3e-4)

        self.critic = Critic(state_dim, action_dim).to(device)
        self.critic_target = copy.deepcopy(self.critic)
        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(), lr=3e-4)

        self.max_action = max_action
        self.discount = discount
        self.tau = tau
        self.policy_noise = policy_noise
        self.noise_clip = noise_clip
        self.policy_freq = policy_freq
        self.alpha = alpha

        self.total_it = 0

    def select_action(self, state):
        seq_length = torch.LongTensor([1])
        state = torch.FloatTensor(state.reshape(1, -1)).to(device)
        state = torch.unsqueeze(state, 0)
        return self.actor(state, seq_length).cpu().data.numpy().flatten()

    def train(self, replay_buffer, batch_size=256, history_length=None):
        self.total_it += 1

        # Sample replay buffer
        (
            state,
            action,
            next_state,
            reward,
            not_done,
            state_history,
            action_history,
            next_state_history,
            seq_lengths,
        ) = replay_buffer.sample_history(batch_size, history_length)

        with torch.no_grad():
            # Select action according to policy and add clipped noise
            noise = (torch.randn_like(action_history) * self.policy_noise).clamp(
                -self.noise_clip, self.noise_clip
            )
            next_action_history = (
                self.actor_target(next_state_history, seq_lengths) + noise
            ).clamp(-self.max_action, self.max_action)

            # Compute the target Q value
            target_Q1, target_Q2 = self.critic_target(
                next_state_history, next_action_history, seq_lengths
            )
            target_Q = torch.min(target_Q1, target_Q2)
            target_Q = reward + not_done * self.discount * target_Q

        # Get current Q estimates
        current_Q1, current_Q2 = self.critic(state_history, action_history, seq_lengths)

        # Compute critic loss
        critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
            current_Q2, target_Q
        )

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        # Delayed policy updates
        if self.total_it % self.policy_freq == 0:

            # Compute actor loss
            pi = self.actor(state_history, seq_lengths)
            Q = self.critic.Q1(state_history, pi, seq_lengths)
            lmbda = self.alpha / Q.abs().mean().detach()

            actor_loss = -lmbda * Q.mean() + F.mse_loss(pi, action_history)

            # Optimize the actor
            self.actor_optimizer.zero_grad()
            actor_loss.backward()
            self.actor_optimizer.step()

            # Update the frozen target models
            for param, target_param in zip(
                self.critic.parameters(), self.critic_target.parameters()
            ):
                target_param.data.copy_(
                    self.tau * param.data + (1 - self.tau) * target_param.data
                )

            for param, target_param in zip(
                self.actor.parameters(), self.actor_target.parameters()
            ):
                target_param.data.copy_(
                    self.tau * param.data + (1 - self.tau) * target_param.data
                )

    def save(self, filename):
        torch.save(self.critic.state_dict(), filename + "_critic")
        torch.save(self.critic_optimizer.state_dict(), filename + "_critic_optimizer")

        torch.save(self.actor.state_dict(), filename + "_actor")
        torch.save(self.actor_optimizer.state_dict(), filename + "_actor_optimizer")

    def load(self, filename):
        self.critic.load_state_dict(torch.load(filename + "_critic"))
        self.critic_optimizer.load_state_dict(
            torch.load(filename + "_critic_optimizer")
        )
        self.critic_target = copy.deepcopy(self.critic)

        self.actor.load_state_dict(torch.load(filename + "_actor"))
        self.actor_optimizer.load_state_dict(torch.load(filename + "_actor_optimizer"))
        self.actor_target = copy.deepcopy(self.actor)
