import numpy as np
import torch
import gym
import argparse
import os
import d4rl

import utils
import TD3_BC_POMDP

# Runs policy for X episodes and returns D4RL score
# A fixed seed is used for the eval environment
def eval_policy(
    policy, env_name, seed, pomdp_wrapper, mean, std, seed_offset=100, eval_episodes=10
):
    if pomdp_wrapper != "none":
        eval_env = utils.OfflineEnvPOMDPWrapper(env_name, pomdp_wrapper)
    else:
        eval_env = gym.make(env_name)
    eval_env.seed(seed + seed_offset)

    avg_reward = 0.0
    for _ in range(eval_episodes):
        state, done = eval_env.reset(), False
        while not done:
            state = (np.array(state).reshape(1, -1) - mean) / std
            action = policy.select_action(state)
            state, reward, done, _ = eval_env.step(action)
            avg_reward += reward

    avg_reward /= eval_episodes
    d4rl_score = eval_env.get_normalized_score(avg_reward) * 100

    print("---------------------------------------")
    print(
        f"Evaluation over {eval_episodes} episodes: {avg_reward:.3f}, D4RL score: {d4rl_score:.3f}"
    )
    print("---------------------------------------")
    return d4rl_score


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    # Experiment
    parser.add_argument("--policy", default="TD3_BC_POMDP")  # Policy name
    parser.add_argument(
        "--env", default="hopper-medium-v0"
    )  # OpenAI gym environment name
    parser.add_argument(
        "--seed", default=0, type=int
    )  # Sets Gym, PyTorch and Numpy seeds
    parser.add_argument(
        "--pomdp_wrapper", default="remove_velocity"
    )  # Sets the type of POMDP wrapper for environment and dataset
    parser.add_argument(
        "--eval_freq", default=5e3, type=int
    )  # How often (time steps) we evaluate
    parser.add_argument(
        "--max_timesteps", default=1e6, type=int
    )  # Max time steps to run environment
    parser.add_argument(
        "--save_model", action="store_true"
    )  # Save model and optimizer parameters
    parser.add_argument(
        "--load_model", default=""
    )  # Model load file name, "" doesn't load, "default" uses file_name
    # TD3
    parser.add_argument(
        "--expl_noise", default=0.1
    )  # Std of Gaussian exploration noise
    parser.add_argument(
        "--batch_size", default=256, type=int
    )  # Batch size for both actor and critic
    parser.add_argument(
        "--history_length", default=None, type=int
    )  # History length used for POMDP
    parser.add_argument("--discount", default=0.99)  # Discount factor
    parser.add_argument("--tau", default=0.005)  # Target network update rate
    parser.add_argument(
        "--policy_noise", default=0.2
    )  # Noise added to target policy during critic update
    parser.add_argument(
        "--noise_clip", default=0.5
    )  # Range to clip target policy noise
    parser.add_argument(
        "--policy_freq", default=2, type=int
    )  # Frequency of delayed policy updates
    # TD3 + BC
    parser.add_argument("--alpha", default=2.5)
    parser.add_argument("--normalize", default=True)
    args = parser.parse_args()

    file_name = f"{args.policy}_{args.env}_{args.pomdp_wrapper}_{args.seed}"
    print("---------------------------------------")
    print(
        f"Policy: {args.policy}, Env: {args.env}, POMDP Wrapper: {args.pomdp_wrapper}, Seed: {args.seed}"
    )
    print("---------------------------------------")

    if not os.path.exists("./results"):
        os.makedirs("./results")

    if args.save_model and not os.path.exists("./models"):
        os.makedirs("./models")

    if args.pomdp_wrapper != "none":
        env = utils.OfflineEnvPOMDPWrapper(args.env, args.pomdp_wrapper)
    else:
        env = gym.make(args.env)

    # Set seeds
    env.seed(args.seed)
    env.action_space.seed(args.seed)
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)

    state_dim = env.observation_space.shape[0]
    action_dim = env.action_space.shape[0]
    max_action = float(env.action_space.high[0])

    kwargs = {
        "state_dim": state_dim,
        "action_dim": action_dim,
        "max_action": max_action,
        "discount": args.discount,
        "tau": args.tau,
        # TD3
        "policy_noise": args.policy_noise * max_action,
        "noise_clip": args.noise_clip * max_action,
        "policy_freq": args.policy_freq,
        # TD3 + BC
        "alpha": args.alpha,
    }

    # Initialize policy
    policy = TD3_BC_POMDP.TD3_BC_POMDP(**kwargs)

    if args.load_model != "":
        policy_file = file_name if args.load_model == "default" else args.load_model
        policy.load(f"./models/{policy_file}")

    replay_buffer = utils.ReplayBuffer(state_dim, action_dim)
    if args.pomdp_wrapper != "none":
        dataset = d4rl.qlearning_dataset(env)
        po_dataset = utils.offline_dataset_pomdp_wrapper(
            dataset, args.env, args.pomdp_wrapper, q_learning=True
        )
        replay_buffer.convert_D4RL(po_dataset)
    else:
        replay_buffer.convert_D4RL(d4rl.qlearning_dataset(env))
    if args.normalize:
        mean, std = replay_buffer.normalize_states()
    else:
        mean, std = 0, 1

    evaluations = []
    for t in range(int(args.max_timesteps)):
        policy.train(replay_buffer, args.batch_size, args.history_length)
        # Evaluate episode
        if (t + 1) % args.eval_freq == 0:
            print(f"Time steps: {t+1}")
            evaluations.append(
                eval_policy(policy, args.env, args.seed, args.pomdp_wrapper, mean, std)
            )
            np.save(f"./results/{file_name}", evaluations)
            if args.save_model:
                policy.save(f"./models/{file_name}")
