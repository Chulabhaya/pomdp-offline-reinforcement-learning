import numpy as np
import torch
import gym
import copy
from torch.nn.utils.rnn import pad_sequence


class ReplayBuffer(object):
    def __init__(self, state_dim, action_dim, max_size=int(1e6)):
        self.max_size = max_size
        self.ptr = 0
        self.size = 0

        self.state = np.zeros((max_size, state_dim))
        self.action = np.zeros((max_size, action_dim))
        self.next_state = np.zeros((max_size, state_dim))
        self.reward = np.zeros((max_size, 1))
        self.not_done = np.zeros((max_size, 1))

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def add(self, state, action, next_state, reward, done):
        self.state[self.ptr] = state
        self.action[self.ptr] = action
        self.next_state[self.ptr] = next_state
        self.reward[self.ptr] = reward
        self.not_done[self.ptr] = 1.0 - done

        self.ptr = (self.ptr + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)

    def sample(self, batch_size):
        ind = np.random.randint(0, self.size, size=batch_size)

        return (
            torch.FloatTensor(self.state[ind]).to(self.device),
            torch.FloatTensor(self.action[ind]).to(self.device),
            torch.FloatTensor(self.next_state[ind]).to(self.device),
            torch.FloatTensor(self.reward[ind]).to(self.device),
            torch.FloatTensor(self.not_done[ind]).to(self.device),
        )

    def sample_history(self, batch_size, history_length=None):
        # Generate random sample indices from dataset
        trajectory_indices = np.random.randint(0, self.size, size=batch_size)

        # Lists for storing trajectories
        trajectories_states = []
        trajectories_actions = []
        trajectories_next_states = []

        # Get locations of all dones
        dones = np.argwhere(self.not_done == 0)[:, 0]
        for i in range(batch_size):
            # Get index
            ind = trajectory_indices[i]

            # Get closest done to index that is less than
            # the index value
            if ind < dones[0]:
                current_episode_start_timestep = 0
            else:
                previous_episode_done_timestep = dones[dones < ind].max()
                current_episode_start_timestep = previous_episode_done_timestep + 1

            # Get full trajectory up to index timestep from start of the episode
            trajectory_state = torch.FloatTensor(
                self.state[current_episode_start_timestep : ind + 1]
            )
            trajectory_action = torch.FloatTensor(
                self.action[current_episode_start_timestep : ind + 1]
            )
            trajectory_next_state = torch.FloatTensor(
                self.next_state[current_episode_start_timestep : ind + 1]
            )

            # Append to proper lists
            trajectories_states.append(trajectory_state)
            trajectories_actions.append(trajectory_action)
            trajectories_next_states.append(trajectory_next_state)

        # Create padded arrays of history
        seq_lengths = torch.LongTensor(list(map(len, trajectories_states)))
        trajectories_states = pad_sequence(trajectories_states).to(self.device)
        trajectories_actions = pad_sequence(trajectories_actions).to(self.device)
        trajectories_next_states = pad_sequence(trajectories_next_states).to(
            self.device
        )

        # Create batches of data with no history
        states = torch.FloatTensor(self.state[trajectory_indices]).to(self.device)
        actions = torch.FloatTensor(self.action[trajectory_indices]).to(self.device)
        next_states = torch.FloatTensor(self.next_state[trajectory_indices]).to(
            self.device
        )
        rewards = torch.FloatTensor(self.reward[trajectory_indices]).to(self.device)
        not_dones = torch.FloatTensor(self.not_done[trajectory_indices]).to(self.device)

        return (
            states,
            actions,
            next_states,
            rewards,
            not_dones,
            trajectories_states,
            trajectories_actions,
            trajectories_next_states,
            seq_lengths,
        )

    def convert_D4RL(self, dataset):
        self.state = dataset["observations"]
        self.action = dataset["actions"]
        self.next_state = dataset["next_observations"]
        self.reward = dataset["rewards"].reshape(-1, 1)
        self.not_done = 1.0 - dataset["terminals"].reshape(-1, 1)
        self.size = self.state.shape[0]

    def normalize_states(self, eps=1e-3):
        mean = self.state.mean(0, keepdims=True)
        std = self.state.std(0, keepdims=True) + eps
        self.state = (self.state - mean) / std
        self.next_state = (self.next_state - mean) / std
        return mean, std


def remove_velocity(env_name):
    # Process Gym MuJoCo environments (offline dataset versions)
    if "halfcheetah" in env_name:
        remain_obs_idx = np.arange(0, 8)
    elif "ant" in env_name:
        remain_obs_idx = list(np.arange(0, 13)) + list(np.arange(27, 111))
    elif "walker2d" in env_name:
        remain_obs_idx = np.arange(0, 8)
    elif "hopper" in env_name:
        remain_obs_idx = np.arange(0, 5)
    else:
        raise ValueError("POMDP for {} is not defined!".format(env_name))

    # Redefine observation_space
    obs_low = np.array([-np.inf for i in range(len(remain_obs_idx))], dtype="float32")
    obs_high = np.array([np.inf for i in range(len(remain_obs_idx))], dtype="float32")
    observation_space = gym.spaces.Box(obs_low, obs_high)

    return remain_obs_idx, observation_space


class OfflineEnvPOMDPWrapper(gym.ObservationWrapper):
    def __init__(
        self,
        env_name,
        pomdp_type="remove_velocity",
        flicker_prob=0.2,
        random_noise_sigma=0.1,
        random_sensor_missing_prob=0.1,
    ):
        super().__init__(gym.make(env_name))

        self.pomdp_type = pomdp_type
        self.flicker_prob = flicker_prob
        self.random_noise_sigma = random_noise_sigma
        self.random_sensor_missing_prob = random_sensor_missing_prob

        if pomdp_type == "remove_velocity":
            # Remove Velocity info, comes with the change in observation space.
            self.remain_obs_idx, self.observation_space = remove_velocity(env_name)
        elif pomdp_type == "flickering":
            pass
        elif self.pomdp_type == "random_noise":
            pass
        elif self.pomdp_type == "random_sensor_missing":
            pass
        elif self.pomdp_type == "remove_velocity_and_flickering":
            # Remove Velocity Info, comes with the change in observation space.
            self.remain_obs_idx, self.observation_space = remove_velocity(env_name)
        elif self.pomdp_type == "remove_velocity_and_random_noise":
            # Remove Velocity Info, comes with the change in observation space.
            self.remain_obs_idx, self.observation_space = remove_velocity(env_name)
        elif self.pomdp_type == "remove_velocity_and_random_sensor_missing":
            # Remove Velocity Info, comes with the change in observation space.
            self.remain_obs_idx, self.observation_space = remove_velocity(env_name)
        elif self.pomdp_type == "flickering_and_random_noise":
            pass
        elif self.pomdp_type == "random_noise_and_random_sensor_missing":
            pass
        elif self.pomdp_type == "random_sensor_missing_and_random_noise":
            pass
        else:
            raise ValueError("pomdp_type was not specified!")

    def observation(self, obs):
        # Single source of POMDP
        if self.pomdp_type == "remove_velocity":
            return obs.flatten()[self.remain_obs_idx]
        elif self.pomdp_type == "flickering":
            # Note: flickering is equivalent to:
            #   flickering_and_random_sensor_missing, random_noise_and_flickering, random_sensor_missing_and_flickering
            if np.random.rand() <= self.flicker_prob:
                return np.zeros(obs.shape)
            else:
                return obs.flatten()
        elif self.pomdp_type == "random_noise":
            return (
                obs + np.random.normal(0, self.random_noise_sigma, obs.shape)
            ).flatten()
        elif self.pomdp_type == "random_sensor_missing":
            obs[np.random.rand(len(obs)) <= self.random_sensor_missing_prob] = 0
            return obs.flatten()
        # Multiple source of POMDP
        elif self.pomdp_type == "remove_velocity_and_flickering":
            # Note: remove_velocity_and_flickering is equivalent to flickering_and_remove_velocity
            # Remove velocity
            new_obs = obs.flatten()[self.remain_obs_idx]
            # Flickering
            if np.random.rand() <= self.flicker_prob:
                return np.zeros(new_obs.shape)
            else:
                return new_obs
        elif self.pomdp_type == "remove_velocity_and_random_noise":
            # Note: remove_velocity_and_random_noise is equivalent to random_noise_and_remove_velocity
            # Remove velocity
            new_obs = obs.flatten()[self.remain_obs_idx]
            # Add random noise
            return (
                new_obs + np.random.normal(0, self.random_noise_sigma, new_obs.shape)
            ).flatten()
        elif self.pomdp_type == "remove_velocity_and_random_sensor_missing":
            # Note: remove_velocity_and_random_sensor_missing is equivalent to random_sensor_missing_and_remove_velocity
            # Remove velocity
            new_obs = obs.flatten()[self.remain_obs_idx]
            # Random sensor missing
            new_obs[np.random.rand(len(new_obs)) <= self.random_sensor_missing_prob] = 0
            return new_obs
        elif self.pomdp_type == "flickering_and_random_noise":
            # Flickering
            if np.random.rand() <= self.flicker_prob:
                new_obs = np.zeros(obs.shape)
            else:
                new_obs = obs
            # Add random noise
            return (
                new_obs + np.random.normal(0, self.random_noise_sigma, new_obs.shape)
            ).flatten()
        elif self.pomdp_type == "random_noise_and_random_sensor_missing":
            # Random noise
            new_obs = (
                obs + np.random.normal(0, self.random_noise_sigma, obs.shape)
            ).flatten()
            # Random sensor missing
            new_obs[np.random.rand(len(new_obs)) <= self.random_sensor_missing_prob] = 0
            return new_obs
        elif self.pomdp_type == "random_sensor_missing_and_random_noise":
            # Random sensor missing
            obs[np.random.rand(len(obs)) <= self.random_sensor_missing_prob] = 0
            # Random noise
            return (
                obs + np.random.normal(0, self.random_noise_sigma, obs.shape)
            ).flatten()
        else:
            raise ValueError(
                "pomdp_type was not in ['remove_velocity', 'flickering', 'random_noise', 'random_sensor_missing']!"
            )


def offline_dataset_pomdp_wrapper(
    dataset,
    env_name,
    pomdp_type="remove_velocity",
    flicker_prob=0.2,
    random_noise_sigma=0.1,
    random_sensor_missing_prob=0.1,
    q_learning=False,
):

    new_dataset = copy.deepcopy(dataset)
    obs = new_dataset["observations"]
    if q_learning:
        next_obs = new_dataset["next_observations"]

    # Single source of POMDP
    if pomdp_type == "remove_velocity":
        remain_obs_idx, _ = remove_velocity(env_name)
        obs = obs[:, remain_obs_idx]
        if q_learning:
            next_obs = next_obs[:, remain_obs_idx]

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "flickering":
        # Note: flickering is equivalent to:
        #   flickering_and_random_sensor_missing, random_noise_and_flickering, random_sensor_missing_and_flickering
        num_rows = obs.shape[0]
        num_replaced = int(flicker_prob * num_rows)
        row_idx = np.random.randint(0, num_rows, num_replaced)
        obs[row_idx, :] = 0
        if q_learning:
            next_obs[row_idx, :] = 0

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "random_noise":
        obs = obs + np.random.normal(0, random_noise_sigma, obs.shape)
        if q_learning:
            next_obs = next_obs + np.random.normal(
                0, random_noise_sigma, next_obs.shape
            )

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "random_sensor_missing":
        obs[
            np.random.rand(obs.shape[0], obs.shape[1]) <= random_sensor_missing_prob
        ] = 0
        if q_learning:
            next_obs[
                np.random.rand(next_obs.shape[0], next_obs.shape[1])
                <= random_sensor_missing_prob
            ] = 0

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    # Multiple source of POMDP
    elif pomdp_type == "remove_velocity_and_flickering":
        # Note: remove_velocity_and_flickering is equivalent to flickering_and_remove_velocity
        # Remove velocity
        remain_obs_idx, _ = remove_velocity(env_name)
        obs = obs[:, remain_obs_idx]
        if q_learning:
            next_obs = next_obs[:, remain_obs_idx]
        # Flickering
        num_rows = obs.shape[0]
        num_replaced = int(flicker_prob * num_rows)
        row_idx = np.random.randint(0, num_rows, num_replaced)
        obs[row_idx, :] = 0
        if q_learning:
            next_obs[row_idx, :] = 0

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "remove_velocity_and_random_noise":
        # Note: remove_velocity_and_random_noise is equivalent to random_noise_and_remove_velocity
        # Remove velocity
        remain_obs_idx, _ = remove_velocity(env_name)
        obs = obs[:, remain_obs_idx]
        if q_learning:
            next_obs = next_obs[:, remain_obs_idx]
        # Add random noise
        obs = obs + np.random.normal(0, random_noise_sigma, obs.shape)
        if q_learning:
            next_obs = next_obs + np.random.normal(
                0, random_noise_sigma, next_obs.shape
            )

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "remove_velocity_and_random_sensor_missing":
        # Note: remove_velocity_and_random_sensor_missing is equivalent to random_sensor_missing_and_remove_velocity
        # Remove velocity
        remain_obs_idx, _ = remove_velocity(env_name)
        obs = obs[:, remain_obs_idx]
        if q_learning:
            next_obs = next_obs[:, remain_obs_idx]
        # Random sensor missing
        obs[
            np.random.rand(obs.shape[0], obs.shape[1]) <= random_sensor_missing_prob
        ] = 0
        if q_learning:
            next_obs[
                np.random.rand(next_obs.shape[0], next_obs.shape[1])
                <= random_sensor_missing_prob
            ] = 0

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "flickering_and_random_noise":
        # Flickering
        num_rows = obs.shape[0]
        num_replaced = int(flicker_prob * num_rows)
        row_idx = np.random.randint(0, num_rows, num_replaced)
        obs[row_idx, :] = 0
        if q_learning:
            next_obs[row_idx, :] = 0
        # Add random noise
        obs = obs + np.random.normal(0, random_noise_sigma, obs.shape)
        if q_learning:
            next_obs = next_obs + np.random.normal(
                0, random_noise_sigma, next_obs.shape
            )

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "random_noise_and_random_sensor_missing":
        # Random noise
        obs = obs + np.random.normal(0, random_noise_sigma, obs.shape)
        if q_learning:
            next_obs = next_obs + np.random.normal(
                0, random_noise_sigma, next_obs.shape
            )
        # Random sensor missing
        obs[
            np.random.rand(obs.shape[0], obs.shape[1]) <= random_sensor_missing_prob
        ] = 0
        if q_learning:
            next_obs[
                np.random.rand(next_obs.shape[0], next_obs.shape[1])
                <= random_sensor_missing_prob
            ] = 0

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    elif pomdp_type == "random_sensor_missing_and_random_noise":
        # Random sensor missing
        obs[
            np.random.rand(obs.shape[0], obs.shape[1]) <= random_sensor_missing_prob
        ] = 0
        if q_learning:
            next_obs[
                np.random.rand(next_obs.shape[0], next_obs.shape[1])
                <= random_sensor_missing_prob
            ] = 0
        # Random noise
        obs = obs + np.random.normal(0, random_noise_sigma, obs.shape)
        if q_learning:
            next_obs = next_obs + np.random.normal(
                0, random_noise_sigma, next_obs.shape
            )

        new_dataset["observations"] = obs
        new_dataset["next_observations"] = next_obs
        return new_dataset
    else:
        raise ValueError(
            "pomdp_type was not in ['remove_velocity', 'flickering', 'random_noise', 'random_sensor_missing']!"
        )
