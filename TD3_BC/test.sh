#!/bin/bash

# Script to reproduce results

envs=(
        "halfcheetah-medium-expert-v0"
        "hopper-medium-expert-v0"
        "walker2d-medium-expert-v0"
        )
pomdp_wrappers=(
        "remove_velocity"
        # "flickering"
        # "random_noise"
        # "random_sensor_missing"
        )
for ((i=0;i<5;i+=1))
do
        for env in ${envs[*]}
        do
                for pomdp_wrapper in ${pomdp_wrappers[*]}
                do
                        python main_pomdp.py \
                        --env $env \
                        --seed $i \
                        --pomdp_wrapper $pomdp_wrapper
                done
        done
done